<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <base href="assets/">

    <meta name="theme-color" content="#ff823d">
    <link rel="icon" type="image/png" href="img/favicon.png">
    <link rel="icon" sizes="192x192" href="img/favicon.png">

    <title>Total Service - Website em desenvolvimento</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=PT+Sans' rel='stylesheet' type='text/css'>

    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>

    <?if(isset($_GET['action']) && $_GET['action'] == 'ok'):?>
      <p class="mail mail-ok">
        <span class="glyphicon glyphicon-ok"></span>
        Sucesso! Sua mensagem foi enviada, at&eacute; breve.
      </p>
    <?endif;?>

    <?if(isset($_GET['action']) && $_GET['action'] == 'err'):?>
      <p class="mail mail-err">
        <span class="glyphicon glyphicon-remove"></span>
        Ops! Houve um erro ao processar sua mensagem... Por favor, tente novamente.
      </p>
    <?endif;?>

    <div class="container">
      <div class="row row-centered">
        <div class="col-xs-11 col-sm-9 col-md-9 col-centered">

          <div class="row">

            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-5">
              <ul class="list-centered">
                <li><img src="img/logo.png" alt="Total Service" title="Total Service"></li>
                <li>
                  <h1>
                    Especializada em instalação e manutenção de sistemas eletrônicos e elétrica.
                  </h1> 
                </li>
                <li>
                  <a href="tel:1933886760">(19) 3388-6760</a> ou
                  <!-- <a href="tel:19997255721">(19) 99725-5721</a> / -->
                  <a href="tel:19974046244">97404-6244</a>
                </li>
                <li><a href="mailto:contato@tsassistencia.com.br?subject=Contato site TS: Página de espera" title="Será um prazer atendê-lo!">contato@tsassistencia.com.br</a></li>
              </ul>
            </div>

            <div class="col-xs-12 col-sm-6 col-md-6">
              <form id="front-contact" action="mail/sendmail.php" method="post">
                <div class="form-group">
                  <label for="front_name">Seu nome ou empresa</label>
                  <input type="text" id="front_name" name="front_name" class="form-control" required minlength="3">
                </div>
                <div class="form-group">
                  <label for="front_email">Seu e-mail para contato</label>
                  <input type="email" id="front_email" name="front_email" class="form-control" required>
                </div>
                <div class="form-group">
                  <label for="front_message">Mensagem</label>
                  <textarea name="front_message" id="front_message" class="form-control" required minlength="5"></textarea>
                </div>
                <button type="submit" class="btn btn-primary">
                  <span class="glyphicon glyphicon-earphone"></span>&nbsp;&nbsp;Solicitar contato
                </button>
              </form>
            </div>

          </div>

        </div>
      </div>
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.13.1/jquery.validate.min.js"></script>

    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
      ga('create', 'UA-63608571-1', 'auto');
      ga('send', 'pageview');
    </script>

    <script>
      $().ready(function() {
      	var validator = $("#front-contact").validate({
      		errorPlacement: function(error, element) {
      			$( element )
      				.closest( "form" )
      					.find( "label[for='" + element.attr( "id" ) + "']" )
      						.append( error );
      		},
      		errorElement: "span",
      		messages: {
            front_name: {
      				required: " (obrigatório)",
      				minlength: " (mínimo 3 caracteres)"
      			},
            front_email: {
      				required: " (obrigatório)",
              email: " (inválido)"
      			},
      			front_message: {
      				required: " (obrigatório)",
      				minlength: " (mínimo 10 caracteres)"
      			}
      		}
      	});
      });
    </script>

  </body>
</html>

<!--
  Desenvolvido por Sitefy, serviço expresso de 
  desenvolvimento, manutenção e suporte de websites.

  www.sitefy.com.br / suporte@sitefy.com.br / 19 99329-2025
-->
